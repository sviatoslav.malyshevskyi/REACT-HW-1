import React from 'react';
import '../../../assets/fonts/fonts.css';
import '../../../shared/styles/css/style.css';

import {Header} from "../../../client/components/header/components/Header";
import {MoviesWrapper} from "../../../client/components/movies/components";
import {Footer} from "../../../client/components/footer/components/Footer";
import headerProps from "../../../client/components/header/components/headerProps/headerProps";
import moviesProps from "../../../client/components/movies/components/MoviesProps/moviesProps";
import footerProps from "../../../client/components/footer/components/footerProps/footerProps";

export const App = () => {
    const {...header} = headerProps;
    const {...movies} = moviesProps;
    const {...footer} = footerProps;

    return (
        <div className={"container"}>
            <Header {...header} />
            <MoviesWrapper {...movies}/>
            <Footer {...footer} />
        </div>
    )
};
