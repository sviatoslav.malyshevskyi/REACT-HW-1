import React from 'react';
import { ExamplePage } from '../../client/example/pages';

export const AppRoutes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <ExamplePage />
      </Route>
    </Switch>
  );
};
