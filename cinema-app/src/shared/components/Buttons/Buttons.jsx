import React from 'react';
import './Buttons.css';

const buttonsTypes = {
    primary: "btn-primary",
    viewInfo: "btn-view-info",
    transparent: "btn-transparent"
};

export const Buttons = (props) => {
    const {type, text} = props;
    const className = `btn ${buttonsTypes[type]}`;
    return (
        <button className={className}>{text}</button>
    );
};
