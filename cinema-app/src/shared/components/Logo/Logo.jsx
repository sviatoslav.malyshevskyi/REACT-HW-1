import React from 'react';
import './Logo.css';

export const Logo = (props) => {
    const {firstWord, secondWord} = props;

    return (
        <a className="logo" href="index.js">
            {firstWord}
            {secondWord}
        </a>
    )
};
