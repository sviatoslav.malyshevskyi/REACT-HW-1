import React from 'react';
import './MovieCategories.css';

export const MovieCategories = (props) => {
    const categoriesList = Object.values(props).map((element, index) =>
        <li className="categ-item" key={index}>{element}{props.separ}</li>)
    return (
        <ul className="categories-list">
            {categoriesList}
        </ul>
    )
};
