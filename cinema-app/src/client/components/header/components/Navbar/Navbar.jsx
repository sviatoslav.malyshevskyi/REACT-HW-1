import React from 'react';
import './Navbar.css';
import {Logo} from "../../../../../shared/components/Logo";
import {Buttons} from "../../../../../shared/components/Buttons";

export const Navbar = (props) => {
    const {logo} = props;

    return (
        <div className={'navbar-container'}>
            <Logo {...logo}/>

            <div className={"authentication-container"}>
                <Buttons text="Sign in" type="transparent"/>
                <Buttons text="Sign Up" type="primary"/>
            </div>
        </div>
    )
};
