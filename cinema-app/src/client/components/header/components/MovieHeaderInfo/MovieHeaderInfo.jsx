import React from 'react';
import './MovieHeaderInfo.css';
import {Buttons} from "../../../../../shared/components/Buttons";

export const MovieHeaderInfo = (props) => {
    const {title, categories, duration, rating} = props;
    const categoriesItems = categories.map(item =>
        <li key={item} className="categories-item">{item}</li>);
    const durationH = Math.floor(duration/60);
    const durationM = duration%60;
    const movieDuration = `${durationH}h ${durationM}m`;
    const ratingStars = Array(Math.round(rating)).fill(<span className="stars">&#9733;</span>);

    return (
        <div className={'movie-header-info--wrapper'}>
            <div className={"movie-header-info--container"}>
                <h1 className="movie-title">{title}</h1>
                <div>
                    <ul className="categories-rating-duration-row">
                        {categoriesItems}
                        <li className="duration">|</li>
                        {movieDuration}
                    </ul>
                </div>
                <div className={"rating-stars"}>
                    {ratingStars}
                    <span className="rating">{rating}</span>
                </div>
            </div>

            <div className={"movie-header-info-buttons--container"}>
                <Buttons text = "Watch Now" type="primary" />
                <Buttons text = "View Info" type="viewInfo" />
                <Buttons text = "+ Favorites" type="transparent" />
            </div>
        </div>
    );
};
