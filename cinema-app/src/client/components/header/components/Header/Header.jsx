import React from 'react';
import './Header.css';
import {Navbar} from "../Navbar";
import {MovieHeaderInfo} from "../MovieHeaderInfo";

export const Header = (props) => {
    const {movieHeaderInfo, ...logo} = props;

    return (
        <header className='header'>
            <Navbar {...logo} />
            <MovieHeaderInfo {...movieHeaderInfo} />
        </header>
    )
};
