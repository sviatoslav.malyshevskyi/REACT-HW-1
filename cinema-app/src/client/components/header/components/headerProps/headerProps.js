import React from 'react';

const headerProps = {
    logo: {
        firstWord: <h1 className='logo-first-word'>movie</h1>,
        secondWord: <span className='logo-second-word'>rise</span>,
    },

movieHeaderInfo: {
    title: 'The Jungle Book',
        categories: [
        'Adventure',
        'Drama',
        'Family',
        'Fantasy',
    ],
        duration: 106,
        rating: 4.8,
},
};

export default headerProps;
