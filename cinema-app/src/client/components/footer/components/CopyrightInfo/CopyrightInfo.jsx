import React from 'react';
import './CopyrightInfo.css';

export const CopyrightInfo = (props) => {
    const {copyrightStart, copyrightEnd} = props;

    return (
        <div className={"copyright"}>
            {copyrightStart}
            {copyrightEnd}
        </div>
    );
};
