import React from 'react';
import './SocialIcons.css';

export const SocialIcons = () => {
    return (
        <div className={"social-icons-container"}>
            <a href="https://facebook.com/" className="social-icons-item"><i className="fab fa-facebook-f" /></a>
            <a href="https://twitter.com" className="social-icons-item"><i className="fab fa-twitter" /></a>
            <a href="https://pinterest.com" className="social-icons-item"><i className="fab fa-pinterest-p" /></a>
            <a href="https://instagram.com" className="social-icons-item"><i className="fab fa-instagram" /></a>
            <a href="https://youtube.com" className="social-icons-item"><i className="fab fa-youtube" /></a>
        </div>
    );
};
