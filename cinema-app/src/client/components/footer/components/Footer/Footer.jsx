import React from 'react';
import './Footer.css';
import {FooterMenu} from "../FooterMenu";
import {Logo} from "../../../../../shared/components/Logo";
import {SocialIcons} from "../SocialIcons";
import {CopyrightInfo} from "../CopyrightInfo";

export const Footer = (props) => {
    const {footerLogo, footerCopyright, ...footerMenuItems} = props;
    return (
        <div className={"footer-wrapper"}>
            <div className={"footer-nav"}>
                <div className={"menu-footer"}>
                    <FooterMenu {...footerMenuItems} />
                </div>
                <div className={"logo-footer"}>
                    <Logo {...footerLogo} />
                </div>
                <div className={"social-icons-footer"}>
                    <SocialIcons />
                </div>
            </div>
            <div className={"copyright-container"}>
                <CopyrightInfo {...footerCopyright} />
            </div>
        </div>
    );
};
