import React from "react";

const footerProps = {
    footerMenu: [
        'About',
        'Terms of Service',
        'Contact',
    ],

    footerLogo: {
        firstWord: <h1 className='logo-first-word'>movie</h1>,
        secondWord: <span className='logo-second-word'>rise</span>,
    },

    footerCopyright: {
        copyrightStart: <p>Copyright &#169; 2017 <span className={'text-bold'}>MOVIE</span><span className={"text-thin"}>RISE</span>.</p>,
        copyrightEnd: <p>All Rights Reserved.</p>,
    },
};

export default footerProps;
