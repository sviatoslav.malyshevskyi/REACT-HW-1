import React from 'react';
import './FooterMenu.css';
import {FooterMenuItem} from "./FooterMenuItem";

export const FooterMenu = (props) => {
    const {...footerMenuItems} = props;
    return(
        <div className={"footer-menu-container"}>
            <FooterMenuItem {...footerMenuItems} />
        </div>
    );
};
