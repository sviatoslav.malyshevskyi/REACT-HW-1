import React from 'react';
import './FooterMenuItem.css';

export const FooterMenuItem = (props) => {
    const {footerMenu} = props;
    const footerMenuItems = footerMenu.map(item =>
        <a href="/" className="footer-menu-item-links">
            <li>
                {item}
            </li>
        </a>
    );

    return (
        <div className={"footer-menu-item"}>
            {footerMenuItems}
        </div>
    )
};
