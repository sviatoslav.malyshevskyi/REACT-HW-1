import React from 'react';
import './MoviesWrapper.css'
import {MoviesLayout} from "./MoviesLayout";

export const MoviesWrapper = (props) => {
    return (
        <div className="main-wrapper">
            <MoviesLayout {...props} />
        </div>
    )
};
