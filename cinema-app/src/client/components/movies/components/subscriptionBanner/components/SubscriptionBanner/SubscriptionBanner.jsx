import React from 'react';
import './SubscriptionBanner.css';
import {Buttons} from "../../../../../../../shared/components/Buttons";
import moviesProps from "../../../MoviesProps/moviesProps";

export const SubscriptionBanner = (props) => {
    const {...banner} = props;

    return (
        <div className="banner-container">
            <div className="banner">
                <p className="banner-text">
                    Receive information on the latest hit movies straight to your inbox.
                </p>
                <Buttons text="Subscribe!" type="primary"/>
            </div>
        </div>
    )
};
