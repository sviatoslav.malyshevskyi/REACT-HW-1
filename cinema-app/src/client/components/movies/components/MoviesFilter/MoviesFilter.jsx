import React from 'react';
import './MoviesFilter.css';
import moviesProps from "../MoviesProps/moviesProps";

export const MoviesFilter = () => {
    const moviesFilter = moviesProps.moviesFilter;
    const moviesFilterItems = moviesFilter.map(item =>
        <li className={"movies-filter-items"}>
            {item}
        </li>
    );

    return (
        <div className={"filter-row"}>
            <ul className={"filter-container"}>
                {moviesFilterItems}
            </ul>
        </div>
    );
};
