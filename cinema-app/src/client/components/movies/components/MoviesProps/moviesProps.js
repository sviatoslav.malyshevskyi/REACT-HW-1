import React from "react";

const moviesProps = {
    moviesFilter: [
        'trending',
        'top rated',
        'new arrivals',
        'trailers',
        'coming soon',
        'genre',
    ],

    cards: [{
        title: 'FANTASTIC BEASTS...',
        image: <img src={require("../../../../../assets/images/fantastic-beasts.png")}
                    className="card-image-item"
                    alt="Fantastic Beasts movie" />,
        rating: 4.7,
        categories: ['Adventure', 'Family', 'Fantasy']
    }, {
        title: 'ASSASSIN`S CREED',
        image: <img src={require("../../../../../assets/images/AssasinsCreed.png")}
                    className="card-image-item"
                    alt="Assassin`s Creed movie" />,
        rating: 4.2,
        categories: ['Action', 'Adventure', 'Fantasy']
    }, {
        title: 'NOW YOU SEE ME 2',
        image: <img src={require("../../../../../assets/images/NowYouSeeMe.png")}
                    className="card-image-item"
                    alt="Now You See Me 2 movie" />,
        rating: 4.4,
        categories: ['Action', 'Adventure', 'Comedy']
    }, {
        title: 'THE LEGEND OF TARZAN',
        image: <img src={require("../../../../../assets/images/Tarzan.png")}
                    className="card-image-item"
                    alt="Legend of Tarsan movie" />,
        rating: 4.3,
        categories: ['Action', 'Adventure', 'Drama']
    }, {
        title: 'DOCTOR STRANGE',
        image: <img src={require("../../../../../assets/images/DoctorStrange.png")}
                    className="card-image-item"
                    alt="Doctor Strange movie" />,
        rating: 4.8,
        categories: ['Action', 'Adventure', 'Fantasy']
    }, {
        title: 'CAPTAIN AMERICA ...',
        image: <img src={require("../../../../../assets/images/CaptainAmerica.png")}
                    className="card-image-item"
                    alt="Captain America movie" />,
        rating: 4.9,
        categories: ['Action', 'Adventure', 'Sci-Fi']
    }, {
        title: 'ALICE THROUGH THE LOOKING GLASS',
        image: <img src={require("../../../../../assets/images/Alice.png")}
                    className="card-image-item"
                    alt="Alice Through The Looking Glass movie" />,
        rating: 4.1,
        categories: ['Adventure', 'Family', 'Fantasy']
    }, {
        title: 'FINDING DORY',
        image: <img src={require("../../../../../assets/images/FindingDory.png")}
                    className="card-image-item"
                    alt="Finding Dory movie" />,
        rating: 4.7,
        categories: ['Animation', 'Adventure', 'Comedy']
    }, {
        title: 'THE BFG',
        image: <img src={require("../../../../../assets/images/BFG.png")}
                    className="card-image-item"
                    alt="The BFG movie" />,
        rating: 3.2,
        categories: ['Adventure', 'Family', 'Fantasy']
    }, {
        title: 'INDEPENDENCE DAY',
        image: <img src={require("../../../../../assets/images/IndependenceDay.png")}
                    className="card-image-item"
                    alt="Independence Day movie" />,
        rating: 3.9,
        categories: ['Action', 'Sci-Fi']
    }, {
        title: 'ICE AGE: COLLISION',
        image: <img src={require("../../../../../assets/images/IceAge.png")}
                    className="card-image-item"
                    alt="Ice Age: Collision movie" />,
        rating: 4.5,
        categories: ['Adventure', 'Comedy']
    }, {
        title: 'MOANA',
        image: <img src={require("../../../../../assets/images/Moana.png")}
                    className="card-image-item"
                    alt="Moana movie" />,
        rating: 4.9,
        categories: ['Action', 'Fantasy']
    }]
}

export default moviesProps;
