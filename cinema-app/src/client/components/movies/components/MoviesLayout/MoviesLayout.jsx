import React from 'react';
import './MoviesLayout.css';
import {MovieCard} from "../MovieCard";
import {SubscriptionBanner} from "../subscriptionBanner/components/SubscriptionBanner";
import {MoviesFilter} from "../MoviesFilter";

export const MoviesLayout = (props) => {
    const sectionContent = props.cards.map((element, index) => <MovieCard {...element} key={index} />)
    const rowsAboveBanner = sectionContent.slice(0, 8);
    const rowsBelowBanner = sectionContent.slice(8, 12);

    return (
        <div className="section-cards">
            <MoviesFilter />
            <div className="layout">
                {rowsAboveBanner}
            </div>
            <SubscriptionBanner />
            <div className="layout">
                {rowsBelowBanner}
            </div>
        </div>
    )
};
