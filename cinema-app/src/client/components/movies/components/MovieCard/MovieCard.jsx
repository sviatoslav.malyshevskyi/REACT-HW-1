import React from 'react';
import './MovieCard.css';
import {MovieRating} from "../../../../../shared/components/MovieRating";
import {MovieCategories} from "../../../../../shared/components/MovieCategories";

export const MovieCard = (props) => {
    const {image, title, categories, rating} = props;

    return (
        <div className="card-item">
            <div className="card-image">{image}</div>
            <div className="card-title-and-rating">
                <h5 className="card-title">{title}</h5>
                <MovieRating index={rating} />
            </div>
            <div className="card-categories">
                <MovieCategories {...categories} />
            </div>
        </div>
    )
}
